\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}					%for advanced mathmematical formulas
\usepackage{graphicx}					%always use this package if you want to insert graphics into your report
\usepackage{fullpage}
\usepackage{changepage}% http://ctan.org/pkg/changepage



\graphicspath{{./graphs/}}


% Title Page
\title{\Huge{Augmented Langrangian Method Assignment}\\[7cm]Advanced Design 814\\[2cm]}

\author{\Large Andreas Joss\\[0.5cm]16450612}
\date{\today}

\begin{document}
\maketitle


\newpage
\section{Introduction}
This report briefly explains the Augmented Langrangian Method which is used for this assignment. This method is used to solved constrained optimization problems. This method is implemented in Python code. Various example functions are used to evaluate the successful operation of the code. The results thereof are shown and discussed and is also compared to some of the results from the previous assignment (SUMT).

\section{Theory}
The Augmented Langrangian Method is essentially a combination of the Langrangian approach itself, and the penalty function approach [1].\bigskip

Equation \ref{eq:1} describes the Langrangian function.

\begin{equation}\label{eq:1}
L(\textbf{x},\boldsymbol{\theta},\boldsymbol{\lambda},\boldsymbol{\mu}) = f(\textbf{x}) + \sum_{j=1}^{m} \lambda_{j}(g_{j}(\textbf{x}) + \theta_{j}^{2}) + \sum_{j=1}^{r} \mu_{j}h_{j}(\textbf{x})
\end{equation}

To minimize the Langrangian function (1), the necessary conditions need to be satisfied (that $\textbf{x}^{*},\boldsymbol{\theta}^{*},$ $\boldsymbol{\lambda}^{*},\boldsymbol{\mu}^{*}$ corresponds to a constrained stationary point)[1:62]. The auxiliary variable $\boldsymbol{\theta}$ transforms the inequality constraints to equality constraints. Both the objective function's variables and the dual space variables (Langrangian variables) need to be solved which results in $n+2m+r$ simultaneous non-linear equations where,
\begin{equation*}
n = \text{number of dimensions}
\end{equation*}
\begin{equation*}
m = \text{number of inequality constraints}
\end{equation*}
\begin{equation*}
r = \text{number of equality constraints}
\end{equation*}
These equations are difficult to solve analytically, or in a computational sense also presents extra computational expensiveness [1]. However the Langrangian method makes it possible to find the exact solution of a constrained problem on the condition that the problem is convex. Additionally, if the Hessian of the Lagrange function at $\textbf{x}^{*},\boldsymbol{\theta}^{*},\boldsymbol{\lambda}^{*},\boldsymbol{\mu}^{*}$ is positive definite, then the stationary point is indeed a global minimum [1:64].\bigskip

As discussed in the previous assignment (SUMT), when high accuracy is desired, the penalty parameter needs to be very large which in turn makes the problem ill-conditioned [1]. The advantage of the penalty function method, is that the penalty parameters are not unknown, as they are assumed a magnitude which increases in an iterative manner until a satisfactory result is achieved. Consequently, the penalty parameters would then make it unfavourable for a solution to exist outside of the given constraints.\bigskip

Equation \ref{eq:2} describes the Augmented Langrangian function when only equality constraints are given. When all the Langrangian multipliers $\lambda_{j}$ are chosen equal to zero, the Augmented Langrangian $\mathcal{L}$ becomes the penalty function as was used in the SUMT algorithm. If $\rho_{j}$ are all chosen equal to zero, then the classical Langrangian function remains. Of course the Augmented Langrangian can also be used such that $\rho_{j} = \rho$ for all $j$, as shown by Equation \ref{eq:3}.

\begin{equation}\label{eq:2}
\mathcal{L}(\textbf{x},\boldsymbol{\lambda},\boldsymbol{\rho}) = f(\textbf{x}) + \sum_{j=1}^{r} \lambda_{j}h_{j}(\textbf{x}) + \sum_{j=1}^{r} \rho_{j}h_{j}^{2}(\textbf{x})
\end{equation}

\begin{equation}\label{eq:3}
\mathcal{L}(\textbf{x},\boldsymbol{\lambda},\rho) = f(\textbf{x}) + \sum_{j=1}^{r} \lambda_{j}h_{j}(\textbf{x}) + \rho\sum_{j=1}^{r} h_{j}^{2}(\textbf{x})
\end{equation}

Equation \ref{eq:4} shows the Augmented Langrangian function when inequality constraints are of concern.

\begin{equation}\label{eq:4}
\mathcal{L}(\textbf{x},\boldsymbol{\lambda},\rho) = f(\textbf{x}) + \rho\sum_{j=1}^{m} \bigg \langle \frac{\lambda_{j}}{2\rho} + g_{j}(\textbf{x}) \bigg \rangle^{2}
\end{equation}

where $\langle a \rangle$ denotes max$(a,0)$. The max$(a,0)$ operator represents the same functionality as seen by the inequality penalties of the penalty function in SUMT: when the inequality is not active, then the penalty parameter $\beta$ would become zero, if otherwise then the ``sum operator'' is evaluated and the total penalty function assumes a much larger value.\bigskip

The Augmented Langrangian method (Langrangian multiplier method) uses successive values of $\boldsymbol{\lambda}_{k}$ to approximate $\boldsymbol{\lambda}^{*}$. Because a value is assumed for $\boldsymbol{\lambda}_{k}$, the minimization problem $\mathcal{L}(\textbf{x},\boldsymbol{\lambda}_{k},\boldsymbol{\rho})$ can now be minimized without using too large penalty parameters as with the SUMT process [1:89]. The approximation scheme is derived by comparing the necessary stationary conditions of the Langrangian function with the stationary conditions of the Augmented Langrangian function. The following approximation scheme for $\boldsymbol{\lambda}_{k+1}$ is then obtained [1]:

\begin{equation}\label{eq:5}
 \lambda^{*}_{j} \cong \lambda_{j}^{k+1} = \langle \lambda_{j}^{k} + 2\rho_{k} g_{j}(\textbf{x}^{*k}) \rangle 
\end{equation}

where $\textbf{x}^{*k}$ minimizes the Augmented Langrangian $\mathcal{L}(\textbf{x},\boldsymbol{\lambda}^{k},\rho_{k})$ \bigskip

The strategy for selecting the penalty parameter is the user's choice. For this assignment, the penalty parameter is selected such that
$\rho_{1} = 1$ and thereafter,

\begin{equation}\label{eq:6}
 \rho_{k+1} = 2\rho_{k}
\end{equation}\bigskip

As with the SUMT assignment, the entire algorithm is implemented with the Augmented Langrangian at the most outer loop, the middle loop consists of the Polak-Ribiere Plus Conjugate Gradient method and the most inner loop consists of the Golden Section (1-dimensional line search) method.\bigskip

\section{Results}
The results are displayed using graphs. Within the graphs, two information text boxes are given. Information about the tolerances used, starting position, theoretical solution and Golden Section parameters are given by the upper box. Also included in the upper box is the number of iterations completed (Golden Section, Conjugate Gradient, Augmented Langrangian) and also the time elapsed since the optimization process is started. The lower box indicates the final solution found by the algorithm. The x-axis shows the Augmented Langrangian iteration number. Iteration 0 on this axis represents the value of $f(\textbf{x})$ at the given starting position. The value of $f(\textbf{x})$ (thus without any augmentation or penalty) is shown by the y-axis. Green dots are used to indicate feasible solutions and red dots indicates infeasible solutions. Feasibility is determined by substituting the generated solution into the constraints, and then comparing the value with a small tolerance. The comparison of the substituted constraint function with small tolerances is done because some solutions never quite satisfy the constraint but is very very close of doing so. The coloured dots are also helpful to show if the final solution obtained is indeed feasible or infeasible.\bigskip

The example objective test functions are obtained from [2]. The following results are arbitrary selected example functions and the example numbers (or example names) are named according to the original source [2].

\newpage
\begin{enumerate}
 \item \textit{Example problem 1} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 100(x_{2}-x_{1}^{2})^{2} + (1-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\1 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\1
   \end{bmatrix}
 \end{array}\\
 -1.5 \leq x_{2}$ (unconstrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs/function1.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 1}
 \label{fig:test1}
\end{figure} 

Figure \ref{fig:test1} shows successful convergence of the solution found by the Augmented Langrangian algorithm. Only two Augmented Langrangian iterations are required to reach the termination convergence criteria. This is probably due to the fact that the starting position is already feasible (hence the green dot at iteration 0), which means that the penalties or the cost of being outside the constraints are not significant since the minimum is also not constrained.

\newpage
 \item \textit{Example problem 2} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 100(x_{2}-x_{1}^{2})^{2} + (1-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1.22\\1.5
   \end{bmatrix}
 \end{array}\\
 1.5 \leq x_{2}$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function2.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 2}
 \label{fig:test2}
\end{figure} 

Figure \ref{fig:test2} indicates some minimization but convergence occurs at a point $x = [-1.22,1.50]$ other than the theoretical solution of $x^{*} = [1.22,1.50]$. However the solution is in-fact feasible as $1.5 \leq x_{2}$. It is suspected that a different starting position would lead to the most optimum solution. The graph shows that the starting position is infeasible (red dot). At iteration 1 it is shown that the algorithm minimizes the function quite drastically but after the first iteration the penalty parameter probably makes it to expensive to have an infeasible solution.
 
\newpage
 \item \textit{Example problem 3} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = x_{2} + 10^{-5}(x_{2}-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   10\\1 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   0\\0
   \end{bmatrix}
 \end{array}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function3.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 3}
 \label{fig:test3}
\end{figure} 

Figure \ref{fig:test3} shows some minimization has occurred, with $f^{k}(\textbf{x}) = 0.0010$ very close to $f^{*}(\textbf{x}) = 0$. Also it can be noted that $x_{2}^{k} = x_{2}^{*}$. It is clear that the value of $x_{1}$ has a very limited effect on $f(\textbf{x})$ which explains why the algorithm struggles to find the actual global minimum. By inspection, the function $f(\textbf{x})$ has a term which is scaled by $10^{-5}$ and which is multiplied by the only occurrence of variable $x_{1}$, thereby explaining why this variable is difficult for the algorithm to fine tune.
 
\newpage
 \item \textit{Example problem 4} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = \frac{1}{3}(x_{1} + 1)^{3} + x_{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1.125\\0.125 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\0
   \end{bmatrix}
 \end{array}\\
 1 \leq x_{1}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function4.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 4}
 \label{fig:test4}
\end{figure} 

Figure \ref{fig:test4} shows that the starting position is feasible. The first iteration, shows that when the penalty parameter $\rho$ is very small ($\rho = 1$) the minimization process is not affected that strongly by the constraints, but as the penalty parameter increases by the following iterations, the process becomes more aware of the constraints until finally a feasible solution is found. Notice that this problem has two inequality constraints as compared to the previous three example problems which only has one inequality constraint.
 
\newpage 
 \item \textit{Example problem 5} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = \frac{1}{3}(x_{1} + 1)^{3} + x_{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0\\0 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   -0.55\\-1.55
   \end{bmatrix}
 \end{array}\\
 -1.5 \leq x_{1} \leq 4\\
 -3 \leq x_{2} \leq 3$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function5.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 5}
 \label{fig:test5}
\end{figure} 

Figure \ref{fig:test5} shows a relatively fast convergence where after the theoretical solution is achieved (and thus a feasible solution). Notice that in this problem, two inequalities are given, but in practise these inequalities need to be divided into four separate inequalities.

\newpage 
 \item \textit{Example problem 6} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (1 - x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -1.2\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\1
   \end{bmatrix}
 \end{array}\\
10(x_{2}-x_{1}^{2}) = 0$\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function6.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 6}
 \label{fig:test6}
\end{figure} 

This is the first equality example shown. The starting position is an infeasible solution (red dot). Figure \ref{fig:test6} depicts that the theoretical solution is successfully achieved within the convergence criteria and within the feasible area (green dot). 

\newpage 
 \item \textit{Example problem 12} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 0.5x_{1}^{2} + x_{2}^{2} - x_{1}x_{2} - 7x_{1} - 7x_{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0\\0 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   2\\3
   \end{bmatrix}
 \end{array}\\
 25 - 4x_{1}^{2} - x_{2}^{2} \geq 0$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function12.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 12}
 \label{fig:test12}
\end{figure} 

Notice that this problem has an inequality of quadratic form of both variables $x_{1}$ and $x_{2}$. Figure \ref{fig:test12} shows that the algorithm successfully finds the theoretical global minimum. Again it is visible how the algorithm gradually produces feasible solutions after initially producing infeasible solutions. 
 
\newpage 
 \item \textit{Example problem 13} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - 2)^{2} + x_{2}^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\-2 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\0
   \end{bmatrix}
 \end{array}\\
 (1-x_{1})^{3}-x_{2} \geq 0\\
 0 \leq x_{1}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function13.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 13}
 \label{fig:test13}
\end{figure} 

Figure \ref{fig:test13} suggests that this is a rather difficult problem to solve. This is probably due to the complexity of the given constraints for this particular problem. No feasible solutions are produced. The penalty parameter $\rho$ is limited to $\rho = 50$ to prevent ill-conditioning of the problem.
 
\newpage 
 \item \textit{Example problem 14} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - 2)^{2} + (x_{2}- 1)^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   2\\2 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   0.82\\0.91
   \end{bmatrix}
 \end{array}\\
 -0.25x_{1}^{2}-x_{2}^{2} + 1 \geq 0\\
 x_{1} - 2x_{2} + 1 = 0$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function14.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 14}
 \label{fig:test14}
\end{figure} 

Figure \ref{fig:test14} shows that the algorithm successfully approaches the theoretical solution which is also the first feasible solution produced. It is suspected that more iterations are required to finally generate a feasible solution if the starting position is an infeasible solution. Notice the quadratic nature of the inequality constraint, and additionally the presence of an equality constraint.
 
\newpage 
 \item \textit{Example problem 15} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 100(x_{2} - x_{1}^{2})^{2} + (1-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   0.5\\2
   \end{bmatrix}
 \end{array}\\
 x_{1}x_{2}-1 \geq 0\\
 x_{1} + x_{2}^{2} \geq 0\\
 x_{1} \leq 0.5$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function15.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 15}
 \label{fig:test15}
\end{figure} 

Figure \ref{fig:test15} shows that this problem also proves to be a very challenging optimization problem. For this particular problem, the strategy for selecting the penalty parameter $\rho$ was changed, because after 15 iterations the problem becomes ill-conditioned as the penalty parameter becomes too large. For this problem, the penalty parameter is chosen such that $\rho$ is gradually increased to a maximum of $\rho = 50$. Figure \ref{fig:test15} indicates that a feasible solution is obtained at iteration 35, although it not being the theoretical solution.
 
\newpage 
 \item \textit{Example problem 22} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - 2)^{2} + (x_{2} - 1)^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   2\\2 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\1
   \end{bmatrix}
 \end{array}\\
 -x_{1} - x_{2} + 2 \geq 0\\
 -x_{1}^{2} + x_{2} \geq 0$ (constrained minimum)\bigskip
 
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs/function22.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 22}
 \label{fig:test22}
\end{figure} 

Figure \ref{fig:test22} shows that the Augmented Langrangian process successfully converges to the theoretical solution. The only feasible solution is at iteration 5 when the convergence termination criteria is met.
 
\newpage 
 \item \textit{Example problem 24} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = \frac{1}{27\sqrt{3}}((x_{1} - 3)^{2} - 9)x_{2}^{3}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1\\0.5 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   3\\ \sqrt{3}
   \end{bmatrix}
 \end{array}\\
 x_{1}/\sqrt{3} - x_{2} \geq 0\\
 x_{1} + \sqrt{3}x_{2} \geq 0\\
 -x_{1} - \sqrt{3}x_{2} +6\geq 0\\
 0 \leq x_{1}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip
 
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function24.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 24}
 \label{fig:test24}
\end{figure} 

Figure \ref{fig:test24} shows that successful convergence to the theoretical solution occurs even when five inequality constraints are given.

\newpage 
 \item \textit{Example problem 26} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - x_{2})^{2} + (x_{2}-x_{3})^{4}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1\\0.5 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   3\\ \sqrt{3}
   \end{bmatrix}
 \end{array}\\
(1+x_{2}^{2})x_{1} + x_{3}^{4} - 3 = 0$\bigskip
 
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function26.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 24}
 \label{fig:test26}
\end{figure} 

Figure \ref{fig:test26} shows that the 3-dimensional problem is successfully solved and the solution approaches the theoretical solution.

\newpage 
 \item \textit{Example problem 42} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1}-1)^{2} + (x_{2}-2)^{2} + (x_{3}-3)^{2} + (x_{4}-4)^{2} $; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1\\1\\1\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   2\\2\\0.6\sqrt{2}\\0.8\sqrt{2}
   \end{bmatrix}
 \end{array}\\
 x_{1} - 2 = 0\\
 x_{3}^{2} + x_{4}^{2} - 2 = 0$\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/function42.pdf} 
 \caption{Augmented Langrangian evaluated by example problem 42}
 \label{fig:test42}
\end{figure} 

This example is a 4-dimensional problem with two equality constraints. Figure \ref{fig:test42} indicates that the algorithm produces the theoretical solution and thus also a feasible solution.

\end{enumerate}
\newpage
\section{Conclusion}
The Augmented Langrangian algorithm was successfully implemented in Python code. Constrained example test functions were obtained from [2] and were used to verify the correct operation of the code. Most of the test examples implemented were successfully minimized, in particular example functions 1, 4, 5, 6, 12, 14, 22, 24, 26, 42 all converged to the theoretical solution within the convergence criteria of the algorithm. Only example functions 2, 3, 13 and 15 were not able to be minimized to the global minimum.\bigskip

In general it was found that the Augmented Langrangian method is much more robust than the SUMT algorithm. It should be noted that the exact same set of example functions were used to test the SUMT process. The SUMT algorithm required frequent tuning of the penalty parameter and convergence tolerances to obtain the theoretical solution. On the other hand, the Augmented Langrangian did require much less tuning of the penalty parameter; in-fact tuning was only done on example functions 13 and 15 which in any case did not converge (with or without tuning). Addionally, as expected the penalty parameter remained far smaller than in the SUMT process which adds to the robustness of the Augmented Langrangian method. It is also found that the solutions produced by the Augmented Langrangian had a greater accuracy compared by the solutions generated by the SUMT process. Furthermore, by comparing the elapsed time of each example optimized by the SUMT process vs. the Augmented Langrangian, it does not seem obvious which of the aforementioned methods successfully converges faster.

\bigskip

\section{Bibliography}
\begin{enumerate}
 \item Snyman J. Practical Mathmematical Optimization. Technical report, Department of Mechanical and Aeronautical Engineering, University of Pretoria, Pretoria, South Africa, 2005.
 \item Schittkowski K. Test Examples for Nonlinear Programming Codes, Department of Computer Science, University of Bayreuth, Bayreuth, Germany, 2009.
\end{enumerate}

\end{document} 