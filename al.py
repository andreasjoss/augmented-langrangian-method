#!/usr/bin/python 

#Author:	Andreas Joss (16450612)
#Date:		2015/04/21 

from __future__ import division							#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import sys 
sys.path.append('/home/andreas/Documents/Meesters/AD814/')			#add additional path for interepreter to search for Python modules
import problems as pb								#imports a customized Python module (a Python file which contains variables, functions and classes)
from sympy import *
import pylab as pl
import argparse
import time

		#keep the reload(pr) here until development for pb is completed
reload(pb) 	#reloads the module "pb" as it is only imported the first time it is run from an Python interepreter

example_string = []
example_string.append("Example # 1")
example_string.append("Example # 2")
example_string.append("Example # 3")
example_string.append("Example # 4")
example_string.append("Example # 5")
example_string.append("Example # 6")
example_string.append("Example # 7")
example_string.append("Example # 8")
example_string.append("Example # 9")
example_string.append("Example #10")
example_string.append("Example #11")
example_string.append("Example #12")
example_string.append("Example #13")
example_string.append("Example #14")
example_string.append("Example #15")
example_string.append("Example #16")
example_string.append("Example #17")
example_string.append("Example #18")
example_string.append("Example #19")
example_string.append("Example #20")
example_string.append("Example #21")
example_string.append("Example #22")
example_string.append("Example #23")
example_string.append("Example #24")
example_string.append("Example #25")
example_string.append("Example #26")
example_string.append("Example #27")
example_string.append("Example #28")
example_string.append("Example #29")
example_string.append("Example #30")
example_string.append("Example #31")
example_string.append("Example #32")
example_string.append("Example #33")
example_string.append("Example #34")
example_string.append("Example #35")
example_string.append("Example #36")
example_string.append("Example #37")
example_string.append("Example #38")
example_string.append("Example #39")
example_string.append("Example #40")
example_string.append("Example #41")
example_string.append("Example #42")

###PARSER
##########################################################################################
parser = argparse.ArgumentParser(description="Augmented Langrangian technique",formatter_class=argparse.RawTextHelpFormatter)

#parser.add_argument("direction_choice", help="Select direction type:\n"+
		    #direction_string[0] + "\n"+
		    #direction_string[1] + "\n"+
		    #direction_string[2] + "\n"+
		    #direction_string[3] + "\n", type=int, choices=[1,2,3,4])				#add a new POSITIONAL argument with a help message included

parser.add_argument("choice", help="Select example function to test the Augmented Langrangian technique:\n"#+		#NEED TO CHANGE TO SUMT EXAMPLES
		    #example_string[0] + "\n"+
		    #example_string[1] + "\n"+
		    #example_string[2] + "\n"+
		    #example_string[3] + "\n"+
		    #example_string[4] + "\n"+
		    #example_string[5] + "\n"+
		    #example_string[6] + "\n"+
		    #example_string[7] + "\n"+
		    #example_string[8] + "\n"+
		    #example_string[9] + "\n"
		    ,type=int, choices=[-4,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,22,24,26,28,35,42])						#add a new POSITIONAL argument with a help message included

parser.add_argument("max_al", help="Enter maximum number of Augmented Langrangian iterations allowed", type=int)			#add a new POSITIONAL argument with a help message included

parser.add_argument("max_cg", help="Enter maximum number of conjugate gradient iterations allowed", type=int)			#add a new POSITIONAL argument with a help message included

args = parser.parse_args()									#store the argument parsed into the variable "args"

##########################################################################################

start_time = time.time()

#construct objects for UNCONSTRAINED example problems
#-1 debugging Problem 5.2.6 found on p163 of PM_book.pdf
if args.choice == 0:
  tf = pb.problem("-1 debugging Problem 5.2.6 found on p163 of PM_book.pdf",2)		#instantiates and initializes a new object (a new instance of the "problem" class)
  tf.defineTheoreticalSol([-1,1.5])
  tf.defineStartingPosition([0,0])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)

  f = tf.x[1] - tf.x[2] + 2*tf.x[1]**2 + 2*tf.x[1]*tf.x[2] + tf.x[2]**2

  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
#1 Test function 1
if args.choice == -1:
  tf = pb.problem(example_string[0],2)							#instantiates and initializes a new object (a new instance of the "problem" class)
  tf.defineTheoreticalSol([1,1])
  tf.defineStartingPosition([-1.2,1.0])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)

  f = 100*(tf.x[2] - tf.x[1]**2)**2 + (1-tf.x[1])**2

  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
#2 Test function 2
elif args.choice == -2:
  tf = pb.problem(example_string[1],2)							#instantiates and initializes a new object (a new instance of the "problem" class)
  tf.defineTheoreticalSol([1,3])
  tf.defineStartingPosition([0,0])
  tf.defineGoldenSection(0,4)
  tf.defineConjugateGradientDirection(3)
  
  f = (tf.x[1] + 2*tf.x[2] - 7)**2 + (2*tf.x[1] + tf.x[2] - 5)**2

  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
#3 Test function 3
elif args.choice == -3:
  tf = pb.problem(example_string[2],4)							#instantiates and initializes a new object (a new instance of the "problem" class)
  tf.defineTheoreticalSol([0,0,0,0])
  tf.defineStartingPosition([3,-1,0,1])
  tf.defineGoldenSection(0,2)
  tf.defineConjugateGradientDirection(3)
  
  f = (tf.x[1] + 10*tf.x[2])**2 + 5*(tf.x[3]-tf.x[4])**2 + (tf.x[2]-2*tf.x[3])**4 + 10*(tf.x[1]-tf.x[4])**4

  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al

#this problem is meant for debugging of Augmented Langrangian algorithm
elif args.choice == -4:
  tf = pb.problem("debugging problem 3.5.2.3 found on p110 of PM_book.pdf",2)
  tf.defineTheoreticalSol([3.6363,0.36243])
  tf.defineStartingPosition([0,0])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  #tf.rho = 1
  
  f = tf.x[1]**2 + 10*tf.x[2]**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  
  g = 4 - tf.x[1] - tf.x[2]
  
  tf.addInequalityConstraint(g)
  tf.max_al = args.max_al
  
  pl.yscale('linear')

##CONSTRAINED##
#construct objects for CONSTRAINED example problems
elif args.choice == 1:
  tf = pb.problem("Example problem #1",2)
  tf.defineTheoreticalSol([1,1])
  tf.defineStartingPosition([-2,1])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  #tf.rho = 1
  
  f = 100*(tf.x[2] - tf.x[1]**2)**2 + (1-tf.x[1])**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  
  g = -tf.x[2] - 1.5				#so that g(x) = -x[2] - 1.5  <= 0
  
  tf.addInequalityConstraint(g)
  tf.max_al = args.max_al
  
  pl.yscale('log')

elif args.choice == 2:
  tf = pb.problem("Example problem #2",2)
  a = pl.np.power(598.0/1200.0,0.5)
  b = 400*pl.np.power(a,3)
  tf.defineTheoreticalSol([2*a*pl.np.cos((1.0/3.0)*pl.np.arccos(1.0/b)),1.5])
  tf.defineStartingPosition([-2,1])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  #tf.rho = 1000
  tf.defineConvergenceTolerance (1e-6, 1e-6, 1e-7)
  
  f = 100*(tf.x[2] - tf.x[1]**2)**2 + (1-tf.x[1])**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  
  g = -tf.x[2] + 1.5				#so that g(x) = -x[2] + 1.5  <= 0
  
  tf.addInequalityConstraint(g)
  tf.max_al = args.max_al
  
  pl.yscale('log')

elif args.choice == 3:
  tf = pb.problem("Example problem #3",2)
  tf.defineTheoreticalSol([0,0])
  tf.defineStartingPosition([10,1])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(1)
  #tf.rho = 1000
  
  f = tf.x[2] + (10**(-5))*(tf.x[2]-tf.x[1])**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  
  g = -tf.x[2]					#so that g(x) = -x[2] <= 0
  
  tf.addInequalityConstraint(g)
  tf.max_al = args.max_al
  
  pl.yscale('log')

elif args.choice == 4:
  tf = pb.problem("Example problem #4",2)
  tf.defineTheoreticalSol([1,0])
  tf.defineStartingPosition([1.125,0.125])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  #tf.rho = 100000
  
  f = (1/3)*(tf.x[1] + 1)**3 + tf.x[2]
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  
  g1 = 1 - tf.x[1]				#so that g1(x) = 1 - x[1] <= 0
  g2 = -tf.x[2]					#so that g2(x) = -x[2] <= 0
  
  tf.addInequalityConstraint(g1)
  tf.addInequalityConstraint(g2)
  tf.max_al = args.max_al
  
  pl.yscale('linear')

elif args.choice == 5:
  tf = pb.problem("Example problem #5",2)
  tf.defineTheoreticalSol([-(pl.np.pi/3.0) + 0.5,-(pl.np.pi/3.0) - 0.5])
  tf.defineStartingPosition([0,0])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  
  f = sin(tf.x[1] + tf.x[2]) + (tf.x[1] - tf.x[2])**2 - 1.5*tf.x[1] + 2.5*tf.x[2] + 1
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g1 = tf.x[1] - 4
  g2 = -1.5 - tf.x[1]
  g3 = tf.x[2] - 3
  g4 = -3 - tf.x[2]
  
  tf.addInequalityConstraint(g1)
  tf.addInequalityConstraint(g2)
  tf.addInequalityConstraint(g3)
  tf.addInequalityConstraint(g4)
  
  pl.yscale('linear')

elif args.choice == 6:
  tf = pb.problem("Example problem #6",2)
  tf.defineTheoreticalSol([1,1])
  tf.defineStartingPosition([-1.2,1])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  tf.tolerance_feasible = 1e-3
  
  f = (1-tf.x[1])**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  h = 10*(tf.x[2] - tf.x[1]**2)
  
  tf.addEqualityConstraint(h)
  
  pl.yscale('linear')
  
elif args.choice == 7:
  tf = pb.problem("Example problem #7",2)
  tf.defineTheoreticalSol([0,pl.np.sqrt(3)])
  tf.defineStartingPosition([2,2])
  tf.defineGoldenSection(0,5)
  tf.defineConjugateGradientDirection(3)
  #tf.rho = 100000
  
  #sympy log(x,b) if b is unspecified then the base b=e natural logarithm
  f = log(1+tf.x[1]**2) - tf.x[2]
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  h = (1 + tf.x[1]**2)**2 + tf.x[2]**2 - 4
  
  tf.addEqualityConstraint(h)
  
  pl.yscale('linear')

elif args.choice == 8:
  tf = pb.problem("Example problem #8",2)
  a = pl.np.sqrt((25+pl.np.sqrt(301))/2)
  b = pl.np.sqrt((25-pl.np.sqrt(301))/2)
  
  tf.defineTheoreticalSol([a,9/a])
  
  tf.defineStartingPosition([2,1])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  
  f = -(1**1)
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  h1 = tf.x[1]**2 + tf.x[2]**2 - 25
  h2 = tf.x[1]*tf.x[2] - 9
  
  tf.addEqualityConstraint(h1)
  tf.addEqualityConstraint(h2)
  
  pl.yscale('linear')

elif args.choice == 10:
  tf = pb.problem("Example problem #10",2)
  tf.defineTheoreticalSol([0,1])
  tf.defineStartingPosition([-10,10])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  
  f = tf.x[1] - tf.x[2]
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g = -3*tf.x[1]**2 + 2*tf.x[1]*tf.x[2] - tf.x[2]**2 + 1
  
  tf.addInequalityConstraint(g)
  
  pl.yscale('linear')
  
elif args.choice == 11:
  tf = pb.problem("Example problem #11",2)
  a = 7.5*pl.np.sqrt(6) + pl.np.sqrt(338.5) #a=36.7695
  
  tf.defineTheoreticalSol([(a-1.0/a)/pl.np.sqrt(6),(a**2 - 2 + a**(-2))/6.0])	#x*=[15,225] thus f* = 50700
  tf.defineStartingPosition([4.9,0.1])	#f_start = -1.95
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(1)
  #tf.defineConjugateGradientDirection(3)
  #tf.rho = 100000
  
  f = (tf.x[1] - 5)**2 + tf.x[2]**2 - 25
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g = tf.x[1]**2 - tf.x[2]
  
  tf.addInequalityConstraint(g)
  
  pl.yscale('linear') 
  
elif args.choice == 12:
  tf = pb.problem("Example problem #12",2)
  
  tf.defineTheoreticalSol([2,3])
  tf.defineStartingPosition([0,0])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  
  f = 0.5*tf.x[1]**2 + tf.x[2]**2 - tf.x[1]*tf.x[2] - 7*tf.x[1] - 7*tf.x[2]
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g = -25 + 4*tf.x[1]**2 + tf.x[2]**2
  
  tf.addInequalityConstraint(g)
  
  pl.yscale('linear')   

elif args.choice == 13:
  tf = pb.problem("Example problem #13",2)
  
  tf.defineTheoreticalSol([1,0])
  tf.defineStartingPosition([-2,-2])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(1)
  #tf.defineConjugateGradientDirection(3)
  #tf.rho = 100000
  
  f = (tf.x[1] - 2)**2 + tf.x[2]**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g1 = -(1-tf.x[1])**3 + tf.x[2]
  g2 = -tf.x[1]
  g3 = -tf.x[2]
  
  tf.addInequalityConstraint(g1)
  tf.addInequalityConstraint(g2)
  tf.addInequalityConstraint(g3)
  
  pl.yscale('linear') 

elif args.choice == 14:
  tf = pb.problem("Example problem #14",2)
  
  tf.defineTheoreticalSol([0.5*(pl.np.sqrt(7)-1),0.25*(pl.np.sqrt(7)+1)])	#f*=1.393
  tf.defineStartingPosition([2,2])
  tf.defineGoldenSection(0,1)
  #tf.defineConjugateGradientDirection(1)
  #tf.defineConvergenceTolerance (1e-6, 1e-6, 1e-7)
  tf.defineConjugateGradientDirection(3)
  #tf.rho = 100000
  #tf.tolerance_feasible = 1
  
  f = (tf.x[1] - 2)**2 + (tf.x[2] - 1)**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g1 = 0.25*tf.x[1]**2 + tf.x[2]**2 - 1
  h1 = tf.x[1] - 2*tf.x[2] + 1
  
  tf.addInequalityConstraint(g1)
  tf.addEqualityConstraint(h1)
  
  pl.yscale('linear') 

elif args.choice == 15:
  tf = pb.problem("Example problem #15",2)

  tf.defineTheoreticalSol([0.5,2])
  tf.defineStartingPosition([-2,1])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  #tf.rho = 10000000
  tf.defineConvergenceTolerance (1e-6, 1e-6, 1e-7)
  
  f = 100*(tf.x[2] - tf.x[1]**2)**2 + (1-tf.x[1])**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g1 = 1 - tf.x[1]*tf.x[2]
  g2 = -tf.x[1] - (tf.x[2])**2
  g3 = tf.x[1] - 0.5
  
  tf.addInequalityConstraint(g1)
  tf.addInequalityConstraint(g2)
  tf.addInequalityConstraint(g3)
  
  pl.yscale('log')

elif args.choice == 22:
  tf = pb.problem("Example problem #22",2)
  tf.defineTheoreticalSol([1,1])
  tf.defineStartingPosition([2,2])
  tf.defineGoldenSection(0,3)
  tf.defineConjugateGradientDirection(3)
  
  f = (tf.x[1] - 2)**2 + (tf.x[2] - 1)**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g1 = tf.x[1] + tf.x[2] - 2
  g2 = tf.x[1]**2 - tf.x[2]
  
  tf.addInequalityConstraint(g1)
  tf.addInequalityConstraint(g2)
  
  pl.yscale('linear')

elif args.choice == 24:
  tf = pb.problem("Example problem #24",2)
  tf.defineTheoreticalSol([3,pl.np.sqrt(3)])
  tf.defineStartingPosition([1,0.5])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(3)
  
  f = (1.0/(27.0*sqrt(3.0)))*((tf.x[1] - 3.0)**2 - 9.0)*tf.x[2]**3
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  g1 = tf.x[2] - (tf.x[1]/sqrt(3))
  g2 = -tf.x[1] - sqrt(3)*tf.x[2]
  g3 = tf.x[1] + sqrt(3)*tf.x[2] - 6
  g4 = -tf.x[1]
  g5 = -tf.x[2]
  
  tf.addInequalityConstraint(g1)
  tf.addInequalityConstraint(g2)
  tf.addInequalityConstraint(g3)
  tf.addInequalityConstraint(g4)
  tf.addInequalityConstraint(g5)
  
  pl.yscale('linear')

elif args.choice == 26:
  tf = pb.problem("Example problem #26",3)
  tf.defineTheoreticalSol([1,1,1])
  tf.defineStartingPosition([-2.6,2,2])
  tf.defineGoldenSection(0,1)
  tf.defineConjugateGradientDirection(1)
  #tf.defineConjugateGradientDirection(3)
  tf.defineConvergenceTolerance (1e-6, 1e-6, 1e-7)
  tf.tolerance_feasible = 1e-3
  
  f = (tf.x[1] - tf.x[2])**2 + (tf.x[2] - tf.x[3])**4
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  h = (1+tf.x[2]**2)*tf.x[1] + tf.x[3]**4 - 3
  
  tf.addEqualityConstraint(h)
  
  pl.yscale('linear')

elif args.choice == 28:
  tf = pb.problem("Example problem #28",3)
  tf.defineTheoreticalSol([0.5,-0.5,0.5])
  tf.defineStartingPosition([-4,1,1])
  tf.defineGoldenSection(0,3)
  tf.defineConjugateGradientDirection(3)
  
  f = (tf.x[1] + tf.x[2])**2 + (tf.x[2] + tf.x[3])**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  h = tf.x[1] + 2*tf.x[2] + 3*tf.x[3] - 1
  
  tf.addEqualityConstraint(h)
  
  pl.yscale('linear')

elif args.choice == 42:
  tf = pb.problem("Example problem #42",4)
  tf.defineTheoreticalSol([2,2,0.6*pl.np.sqrt(2),0.8*pl.np.sqrt(2)])
  tf.defineStartingPosition([1,1,1,1])
  tf.defineGoldenSection(0,3)
  tf.defineConjugateGradientDirection(3)
  
  f = (tf.x[1] - 1)**2 +  (tf.x[2] - 2)**2 + (tf.x[3] - 3)**2 + (tf.x[4] - 4)**2
  
  tf.defineFunction(f)
  tf.defineMaxIterations(0,args.max_cg,0)
  tf.max_al = args.max_al
  
  h1 = tf.x[1] -2
  h2 = tf.x[3]**2 + tf.x[4]**2 - 2
  
  tf.addEqualityConstraint(h1)
  tf.addEqualityConstraint(h2)
  
  pl.yscale('linear')
  
  
#execute optimization process  
#tf.doConjugateGradient(3)
#tf.defineConjugateGradientDirection(3)
#tf.doSUMT()
tf.doAugmentedLangrangian()
tf.constructSolutionArray()






#draw output graphs
fig1 = pl.figure(1)
pl.title("f(x) vs. number of iterations executed")

pl.xlabel("Augmented Langrangian iteration number")
pl.ylabel("f(x)")


lines = pl.plot(range(0,tf.itr_al+1+1),tf.f_val_outer)
#pl.setp(lines, color='b', linewidth=2.0)

for i in range (0, tf.itr_al+1+1):
  if tf.feasible_graph[i] == True:
    pl.plot(i, tf.f_val_outer[i], 'g.', markersize=15.0)
  else:
    pl.plot(i, tf.f_val_outer[i], 'r.', markersize=15.0)

'''
previous_i = 0
for i in range (0, tf.itr_al+1):
  if tf.getFeasibility() == True:
    pl.plot(range(previous_i,previous_i+1),tf.f_val_outer[previous_i:previous_i+1][0])
    lines = pl.plot(range(previous_i,previous_i+1),tf.f_val_outer[previous_i:previous_i+1])
    pl.setp(lines, color='g', linewidth=2.0)
    
  else:
    pl.plot(range(previous_i,previous_i+1),tf.f_val_outer[previous_i:previous_i+1])
    lines = pl.plot(range(previous_i,previous_i+1),tf.f_val_outer[previous_i:previous_i+1])
    pl.setp(lines, color='r', linewidth=2.0)
    
  previous_i = previous_i + 1
'''
pl.xticks(pl.np.arange(0, tf.itr_al+1+1+1, 1.0))

axes = pl.gca()
diff = axes.get_ylim()[1] - axes.get_ylim()[0]
axes.set_ylim([axes.get_ylim()[0] - 0.2*diff,axes.get_ylim()[1]+ 0.2*diff])

#pl.yticks(pl.np.arange(0, 2.5, 0.1))

pl.figtext(0.50,0.882, "%s\nEpsilon1(outer): %8.6f\nEpsilon2(outer): %8.6f\nEpsilon(inner)  : %8.6f\nTheoretical solution: %s\nf(x*) = %6.2f\nStarting position: %s\nGolden Section [a,b] = [%.2f,%.2f]\nAugmented Langrangian iterations: %d\nConjugate gradient iterations: %d\nGolden section iterations: %d\nTime elapsed: %.2f seconds" %(example_string[args.choice-1],tf.epsilon1,tf.epsilon2,tf.epsilon_gs,tf.getStringTheoSol(),tf.getCost(tf.theoretical_solution,True),tf.getStringStartPos(),tf.A,tf.B,tf.itr_al+1,tf.total_itr_cg,tf.itr_gs,time.time()-start_time),fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'center',verticalalignment='top')

pl.figtext(0.50,0.562, "Solution\nf(x) = %6.4f\nx = %s"%(tf.f_val[tf.total_itr_cg],tf.getStringEndSol()),fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'center',verticalalignment='top')

l = pl.legend(loc=1, fontsize = 10)# draggable(state=True))

# set the linewidth of each legend object
#for legobj in l.legendHandles:
    #legobj.set_linewidth(2.0)

F = pl.gcf()
F.savefig("./graphs/function%d.pdf" %(args.choice),bbox_inches='tight',format='PDF',pad_inches=0.1)
pl.show() 
